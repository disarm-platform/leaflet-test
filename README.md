# DiSARM leaflet dashboard test

Available at [https://ppls.shinyapps.io/leaflet-test/](https://ppls.shinyapps.io/leaflet-test/)


## Deployment to shinyapps.io

With shinyapps.io account connected, from RStudio run `rsconnect::deployApp()` to start deployment to shinyapps.io


## Shinyapps.io Logs

From RStudio run `rsconnect::showLogs()` to retrieve recent logs. 

MAKING CHANGES
